#!/bin/bash

cpus=$(nproc)

#i=0
#if [ -d /sys/devices/system/cpu/cpu0/cpufreq/ ]; then
#	for i in `seq 0 $cpus`
#		do
#			echo "performance" > /sys/devices/system/cpu/cpu$i/cpufreq/scaling_governor
#		done
#
#else
#	echo "cpu freq folder does not exist!"
#fi

#sudo cat /sys/devices/system/cpu/cpu*/cpufreq/cpuinfo_cur_freq

arch=`dpkg --print-architecture`
echo "Architecture is $arch"

hostname=`awk '{print $1}' /etc/hostname`
cpus=$(nproc)

#OpenMP env vars
#export OMP_BLOCKTIME=0
export OMP_PLACES=cores
export OMP_PROC_BIND=close
#export OMP_NUM_THREADS=$(($cpus/2))

runtime=omp
test_type=(strassen) #(fib fft sort health nqueens)
test_args=(64 128 256 512 1024)
#num_threads=$(($cpus/2)) 
num_threads=(96 192)

for type in "${test_type[@]}"
do
  for t in "${num_threads[@]}"
  do
    summaryfilename=${hostname}-${runtime}-${type}-${t}-time-16k
    echo "Performing tests with $OMP_NUM_THREADS..."
    for args in "${test_args[@]}"
    do
      for run in {1..3}
      do
        OMP_NUM_THREADS=$t ../bin/strassen.clang.omp-tasks -n 16384 -y $args 2>&1 | tee -a $summaryfilename
        #bash run-$type.sh -i $args -c $num_threads 2>&1 | tee -a $summaryfilename
        echo ""
      done
    done
  done
done

'
for type in "${test_type[@]}"
do
  for t in "${num_threads[@]}"
  do
    summaryfilename=${hostname}-${runtime}-${type}-${t}-time
    for args in "${test_args[@]}"
    do
      for run in {1..3}
      do
        OMP_NUM_THREADS=$t ../bin/fib.clang.omp-tasks -n $args 2>&1 | tee -a $summaryfilename
        #bash run-$type.sh -i $args -c $num_threads 2>&1 | tee -a $summaryfilename
        echo ""
      done
    done
  done
done

test_type=(fft sort) #(fib fft sort health nqueens)
test_args=(134217728 268435456 536870912 1073741824)
num_threads=$(($cpus/2)) #(2 4 8 16 32 64 96 128 192)

echo "Performing tests with $OMP_NUM_THREADS..."

for type in "${test_type[@]}"
do
	summaryfilename=scaled-${hostname}-${runtime}-${type}-time-192
	for args in "${test_args[@]}"
  do
	  for run in {1..5}
    do
		  bash run-$type.sh -i $args -c $num_threads 2>&1 | tee -a $summaryfilename
		  echo ""
	  done
  done
done

test_type=(health) #(fib fft sort health nqueens)
test_args=(small.input medium.input large.input)
num_threads=$(($cpus/2)) #(2 4 8 16 32 64 96 128 192)

echo "Performing tests with $OMP_NUM_THREADS..."

for type in "${test_type[@]}"
do
  summaryfilename=scaled-${hostname}-${runtime}-${type}-time-192
  for args in "${test_args[@]}"
  do
    for run in {1..5}
    do
      bash run-$type.sh -i $args -c $num_threads 2>&1 | tee -a $summaryfilename
      echo ""
    done
  done
done

test_type=(nqueens) #(fib fft sort health nqueens)
test_args=(14 15 16)
num_threads=$(($cpus/2)) #(2 4 8 16 32 64 96 128 192)

echo "Performing tests with $OMP_NUM_THREADS..."

for type in "${test_type[@]}"
do
  summaryfilename=scaled-${hostname}-${runtime}-${type}-time-192
  for args in "${test_args[@]}"
  do
    for run in {1..5}
    do
      bash run-$type.sh -i $args -c $num_threads 2>&1 | tee -a $summaryfilename
      echo ""
    done
  done
done
'
