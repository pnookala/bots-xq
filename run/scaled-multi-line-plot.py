#!/usr/bin/python3

import sys
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
import pylab as pl
import csv
import math
import re
from itertools import cycle
from cycler import cycler

def main(args):
	color = ['b','g','c']
	style = ['--','-','-.']
	mark = ['o','*','v']
	#lines = ['bo--', 'g*--', 'cv--'] #, 'co--', 'mo--', 'ro--', 'yo--', 'gv--', 'bv--', 'cv--', 'mv--', 'rv--', 'yv--', 'g*--', 'b*--', 'c*--', 'm*--', 'r*--', 'y*--']	
	stylecycler = cycle(style)
	markcycler = cycle(mark)
	colorcycler = cycle(color)
	#linecycler = cycler('linestyle',['-','--',':'])
	#plt.figure(dpi=300)
	#ax1 = plt.subplot(111)

	# These two lines set-up the color map and tell it to cycle
	#plt.set_cmap('Accent')
	#ax1.set_prop_cycle(cycler('color', ['b', 'g', 'c']) )
	#ax1.set_prop_cycle(cycler('linestyle', ['--', '--', '--']) )
	#ax1.set_prop_cycle(cycler('marker', ['o', '*', 'v']) )
	
	hostname = ["nvmebox1"] #['eightsocket','thunderx2-coresonly',"nvmebox1"]
	w = len(hostname)
	h = 30
	x = []
	y = []
	test_name=["fib","fft","sort","nqueens","health"]
	testtype_print=["OpenMP (iQ - NxN)", "LLVM OpenMP", "GNU OpenMP"]
	test_type=["xq","omp","gomp"]
	
	#num_threads=[["2", "4", "8", "16", "32", "64"],["2", "4", "8", "16", "32", "64", "96", "128", "192"]]

	count = 0
	#x1 = int(sys.argv[2]);
	#y1 = int(sys.argv[3]);
	i = 0;
	yerr = []
	for host in hostname:
		for test in test_name:
			for tt in test_type:
				filename = "scaled-" + host + "-" + tt + "-" + test + "-time"		
				print(filename)
				p = []
				q = []
				p1 = []
				q1 = []
				with open(filename) as fi:
					f = fi.readlines() 
					for line in f:
						if (re.findall("Parameters",line)):
							if test == "health":
								thread = re.findall('([^\/]+$)', line) #for health app
							else:
								thread = re.findall('^\D*(\d+(?:\.\d+)?)', line)
							p.append(thread[0])

						if (re.findall("Time Program*",line)): 
							time = re.findall('([\d.]+)\s+', line)
							q.append(float(time[0]))
				
					p1 = [p[i] for i in range(0,len(p),5)]
					q1 = [sum(q[i:i+5])/5 for i in range(0,len(q), 5)]
					#q1 = [min(q[i:i+5]) for i in range(0,len(q),5)]
					yerr = [np.std(q[i:i+5]) for i in range(0,len(q), 5)]
					print(p1)
					print(q1)
					print(yerr)
						
				i += 1;
				x.append(p1)
				y.append(q1)
			plotname = host + "-" + test + "-time"
			for j in range(0,i):
				xax = np.arange(len(x[j]))
				#plt.plot(x[j], y[j], next(linecycler), label=testtype_print[j])
				plt.errorbar(x[j], y[j], yerr, label=testtype_print[j], linestyle=next(stylecycler), 
										marker=next(markcycler), color=next(colorcycler))

			plt.xlabel("Problem Size (Input)")
			plt.ylabel("Execution Time (Seconds)")
			#plt.xscale('log')
			#plt.xticks(np.arange(len(num_threads[count])), labels=num_threads[count])
			plt.grid(True)
			plt.legend()
			plt.savefig(plotname, dpi=300)
			i = 0;
			x = []
			y = []
			plt.close()
		count = count + 1

if __name__ == "__main__":
    main(sys.argv)
