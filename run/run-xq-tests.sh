#!/bin/bash

cpus=$(nproc)
i=0
if [ -d /sys/devices/system/cpu/cpu0/cpufreq/ ]; then
	for i in `seq 0 $cpus`
		do
			echo "performance" > /sys/devices/system/cpu/cpu$i/cpufreq/scaling_governor
		done

else
	echo "cpu freq folder does not exist!"
fi

sudo cat /sys/devices/system/cpu/cpu*/cpufreq/cpuinfo_cur_freq

arch=`dpkg --print-architecture`
echo "Architecture is $arch"

hostname=`awk '{print $1}' /etc/hostname`
cpus=$(nproc)

#OpenMP env vars
#export OMP_BLOCKTIME=0
export OMP_PLACES=cores
export OMP_PROC_BIND=close
#export OMP_NUM_THREADS=$(($cpus/2))

test_type=(fib fft sort health nqueens)
num_threads=(2 4 8 16 32 64)

echo "Performing tests with $OMP_NUM_THREADS..."

for type in "${test_type[@]}"
do
	summaryfilename=${hostname}-omp-${type}-time
	for t in "${num_threads[@]}"
  do
	  for run in {1..5}
    do
		  bash run-$type.sh -c $t 2>&1 | tee -a $summaryfilename
		  echo ""
	  done
  done
done
