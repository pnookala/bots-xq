#!/usr/bin/python3

import sys
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
import pylab as pl
import csv
import math
import re
from itertools import cycle

def main(args):
	lines = ['bo--', 'g*--'] #, 'co--', 'mo--', 'ro--', 'yo--', 'gv--', 'bv--', 'cv--', 'mv--', 'rv--', 'yv--', 'g*--', 'b*--', 'c*--', 'm*--', 'r*--', 'y*--']	
	linecycler = cycle(lines)
	
	hostname = ["eightsocket"]
	w = len(hostname)
	h = 30
	x = []
	y = []
	test_name=["fib","fft","health","sort","nqueens"]
	testtype_print=["OpenMP (iQ - NxN)", "OpenMP (Original)"]
	test_type=["xq","omp"]
	
	num_threads=[["2", "4", "8", "16", "32", "64"],["2", "4", "8", "16", "32", "64", "96", "128", "192"]]

	count = 0
	#x1 = int(sys.argv[2]);
	#y1 = int(sys.argv[3]);
	i = 0;
	for host in hostname:
		for test in test_name:
			for tt in test_type:
				filename = host + "-" + tt + "-" + test + "-time"		
				print(filename)
				p = []
				q = []
				p1 = []
				q1 = []
				with open(filename) as fi:
					f = fi.readlines() 
					for line in f:
						if (re.findall("# of Threads*",line)):
							thread = re.findall('([\d.]+)\s+', line)
							p.append(thread[0])

						if (re.findall("Time Program*",line)): 
							time = re.findall('([\d.]+)\s+', line)
							q.append(float(time[0]))
				
					p1 = [p[i] for i in range(0,len(p),5)]
					#q1 = [sum(q[i:i+5])/5 for i in range(0,len(q), 5)]
					q1 = [min(q[i:i+5]) for i in range(0,len(q),5)]
					print(p1)
					print(q1)
						
				i += 1;
				x.append(p1)
				y.append(q1)
			plotname = host + "-" + test + "-time"
			for j in range(0,i):
				xax = np.arange(len(x[j]))
				plt.plot(x[j], y[j], next(linecycler), label=testtype_print[j])

			plt.xlabel("# of Threads")
			plt.ylabel("Execution Time (Seconds)")
			#plt.xscale('log')
			plt.xticks(np.arange(len(num_threads[count])), labels=num_threads[count])
			plt.grid(True)
			plt.legend()
			plt.savefig(plotname, dpi=300)
			i = 0;
			x = []
			y = []
			plt.close()
		count = count + 1

if __name__ == "__main__":
    main(sys.argv)
